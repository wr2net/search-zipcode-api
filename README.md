# ZipCode API

### Run Docker Compose

`$ docker-compose up -d` or `$ sh start.sh` 

### Stop Docker Compose
`$ docker-compose down` or `$ sh stop.sh`

### View page in your favorite browser

* List: [http://localhost:8000](http://localhost:8000)

## Postman
Import on Postman Collections and Environment to run actions.

`postman/collections`
`poostman/environments`